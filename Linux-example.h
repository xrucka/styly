#ifndef PB161_EXAMPLE_H
#define PB161_EXAMPLE_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iostream>

class Encoder
{
      private:
	const unsigned BytesIn;
	const unsigned BytesOut;
	const unsigned Modulo;

	virtual uint8_t CodeByte(uint8_t) const = 0;
	virtual uint8_t GetPaddingPlaceholder() const = 0;

      protected:
	Encoder(unsigned _BytesIn, unsigned _BytesOut, unsigned _Modulo)
	    : BytesIn(_BytesIn), BytesOut(_BytesOut), Modulo(_Modulo)
	{
	}

      public:
	void EncodeStream(std::istream &, std::ostream &);

	template <typename InputIterator, typename OutputIterator>
	void Encode(InputIterator ReadCursor, const InputIterator &StopCursor,
		    OutputIterator WriteCursor)
	{
		do {
			uint64_t BlockIn = 0;
			uint8_t BlockOut[BytesOut];
			std::fill(BlockOut, BlockOut + BytesOut, 0);

			// nacte nejvyse i znaku ze vstupu
			unsigned InputCounter = 0;
			unsigned BytesPadded = 0;
			for (InputCounter = 0; (InputCounter < BytesIn) &&
					       (ReadCursor != StopCursor);
			     ++InputCounter)
				BlockIn = (BlockIn << 8) |
					  static_cast<uint8_t>(*(ReadCursor++));

			// zBytek doplni 0 a zapamatuje si, kolik doplnil
			BytesPadded = BytesIn - InputCounter;
			for (unsigned i = 0; i < BytesPadded; ++i)
				BlockIn <<= 8;

			const unsigned RealInput = BytesIn - BytesPadded;
			const unsigned BytesToPad =
			    BytesOut -
			    ((RealInput * BytesOut + BytesIn - 1) / BytesIn);

			unsigned OutputPosition = 0;
			// korektne zpracovany vystup ponacita
			for (OutputPosition = 0; OutputPosition < BytesOut;
			     ++OutputPosition) {
				BlockOut[BytesOut - (OutputPosition + 1)] =
				    CodeByte(BlockIn % Modulo);
				BlockIn /= Modulo;
			}
			// pozice odpovidajici doplnovanym datum predvyplni
			for (OutputPosition = 0; OutputPosition < BytesToPad;
			     ++OutputPosition) {
				BlockOut[BytesOut - (OutputPosition + 1)] =
				    GetPaddingPlaceholder();
			}

			for (unsigned i = 0; i < BytesOut; ++i)
				*(WriteCursor++) = BlockOut[i];

		} while (ReadCursor != StopCursor);
	}
};

struct Base64 : public Encoder {
	Base64() : Encoder(3, 4, 64) {}

      private:
	uint8_t CodeByte(uint8_t Byte) const
	{
		assert(Byte < 64);
		const uint8_t SpecCases[] = {'+', '/'};
		// following function implements the Base64 encoding table
		if (Byte < 26) {
			return Byte - 0 + 'A';
		} else if (Byte < 52) {
			return Byte - 26 + 'a';
		} else if (Byte < 62) {
			return Byte - 52 + '0';
		} else {
			return SpecCases[Byte - 62];
		}
	}
	uint8_t GetPaddingPlaceholder() const { return '='; }
};

struct Ascii85 : public Encoder {
	Ascii85() : Encoder(4, 5, 85) {}

      private:
	uint8_t CodeByte(uint8_t Byte) const { return Byte + 33; }
	uint8_t GetPaddingPlaceholder() const { return CodeByte(0); }
};

#endif // PB161_EXAMPLE_H
