#include "Mesos-example.h"

#include <cstdint>
#include <iterator>
#include <memory>
/**
 * Tato demonstrace je rozsirenim domaci ulohy PB071/jaro2016/HW01.
 * Jejim ucelem je ilustrovat, jak na stejne uloze vypadaji ruzna
 * formatovani zdrojoveho kodu.
 * Nektere prvky C++, ktere pouziva nebudete znat az do konce semestru.
 */

void Encoder::encodeStream(std::istream &in, std::ostream &out) {
  auto flags = in.flags();
  in.unsetf(std::ios_base::skipws);

  std::ostream_iterator<uint8_t> outputCursor(std::cout);
  std::istream_iterator<unsigned char> endCursor;
  std::istream_iterator<unsigned char> inputCursor(std::cin);

  encode(inputCursor, endCursor, outputCursor);

  in.flags(flags);
}

int main(int argc, char **argv) {
  std::unique_ptr<Encoder> encoderInstance;
  if ((argc == 2) && std::string("--base64") == argv[1]) {
    std::unique_ptr<Encoder> b64(new Base64);
    encoderInstance.swap(b64);
  } else {
    std::unique_ptr<Encoder> a85(new Ascii85);
    encoderInstance.swap(a85);
  }

  // neni uplne univerzalni, cin neni otevren v binarnim rezimu
  encoderInstance->encodeStream(std::cin, std::cout);
  std::cout << std::endl;

  return 0;
}
