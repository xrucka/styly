# TENTO REPOZITÁŘ JIŽ NENÍ AKTUÁLNÍ
Aktuální repozitář najdete pod předmětem PB071 -- sice [ZDE](https://gitlab.fi.muni.cz/pb071/codestyles).

Níže uvedené je nicméně starší verzí odkazovaného -- můžete jej tedy použít pro inspiraci.

## Jaký je rozdíl mezi formátováním a analýzou kódu?
Zatím co formátování je "jen" o správném počtu mezer v kódu, je statická analýza věcí správnosti kódu jako takového.

Zatím co valgrind řeší konkrétní běhy programu, s konkrétními vstupy, hledá statická analýza spíše věcné chyby,
místa kde v programu chybí ošetření vstupů, či kde je jiná podobná chyba. Používejte tedy jak valgrind (dynamickou analýzu), tak clang-tidy (či jiný nástroj pro statickou analýzu - tzv. linter).

## Kde získat styly a konfigurace?

Konfigurační soubory jednotlivých stylů najdete v [tomto repozitáři](https://gitlab.fi.muni.cz/pb071/codestyles). Zde budeme publikovat i Vaše styly, které jsme schválili.
Současně zde bude i odpovídající demonstrační příklad, na kterém můžete vidět, jak vypadají nejčastější kousky kódu, formátované konkrétním stylem.

Podobně zde najdete i konfigurační soubory pro clang-tidy (v závislosti na předmětu).

## Jak styl/analyzátor použiji?

Zvolený styl deklarujete (pro automatické odevzdání) souborem codestyle.txt (soubor umístěte vždy do adresáře konkrétní úlohy).
Do tohoto souboru umístěte jediný řádek, sice jméno stylu (tj. např. WebKit). Pro aplikaci tohoto stylu na svůj kód pak soubor
se stylem (např. WebKit.yml) zkopírujte do adresáře úlohy a přejmenujte na _clang-format (popř. .clang-format).
Pak už postupujte podle návodu pro clang-format s konfiguračním souborem (volba -style=file). Může Vám téže přijít vhod volba -i .

Konfiguraci pro clang-tidy nakopírujte do adresáře úlohy a pojmenujte jako .clang-tidy (bez přípony, včetně tečky).
Vlastní analyzátor pak pustíte příkazem:

```sh
clang-tidy -extra-arg-before=-x -extra-arg-before=c -extra-arg=-std=c99 *.c
```

## Jak přihlásím vlastní styl?

Naklonujte či forkněte si repozitář se styly a pošlete nám merge request. Pokud budeme považovat styl za přípustný, Váš commit do repozitáře zařadíme.

## Námi dodané styly

*   PB071 (výchozí styl pro předmět PB071, viz níže)
*   PB161 (výchozí styl pro předmět PB161, viz níže)
*   Linux styl, blížící se pravidlům formátování kódu jádra Linuxu [https://kernel.org](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/process/coding-style.rst)
*   LLVM ([http://llvm.org/docs/CodingStandards.html](http://llvm.org/docs/CodingStandards.html))
*   Google ([https://google.github.io/styleguide/cppguide.html](https://google.github.io/styleguide/cppguide.html))
*   Chromium ([http://www.chromium.org/developers/coding-style](http://www.chromium.org/developers/coding-style))
*   Mozilla ([https://developer.mozilla.org/en-US/docs/Developer_Guide/Coding_Style](https://developer.mozilla.org/en-US/docs/Developer_Guide/Coding_Style))
*   WebKit ([http://www.webkit.org/coding/coding-style.html](http://www.webkit.org/coding/coding-style.html))
*   Mesos (Apache Mesos [http://www.webkit.org/coding/coding-style.html](http://mesos.apache.org/documentation/latest/c++-style-guide))
*   VisualStudio -- tato šablona odráží výchozí formátování ve VisualStudiu

### Styl PB071

Tento styl je odvozen tak, aby se co nejvíce blížil konvencím, [odkazovaným z webu předmětu](https://cecko.eu/public/pb071_konvencec).

### Styl PB161

Tento styl je odvozen od stylu WebKit, avšak s následujícími změnami:

*   Otevírací závorka těla funkce je na stejném řádku, jako hlavička funkce.
*   Krátké bloky kódu a těla funkcí je možné držet v jednom řádku.
*   Inicializační sekce konstruktoru může být na stejném řádku jako hlavička konstruktoru.
*   Mezery kolem * u ukazatelů a & u referencí se sjednotí podle verze, která je v souboru nejpoužívanější. A pokud by bylo skóre vyrovnané, tak se pro jistotu dá mezera doleva i doprava.
*   Odsazuje se tabulátorem, zobrazovaným jako 4 mezery.
*   Vizuální zarovnání pravých stran po sobě jdoucích řádků s přiřazením je povoleno.

```c++
    int velmiDlouhaDemonstracniPromenna = 5;
    int jinaDemonstracniPromenna        = 42;
```

