/** TAKHLE ANO **/
typedef struct node {
	struct node *next;
	int value;
} node_t;

typedef enum mode { Execute = 1, Write = 2, Read = 4 } mode_t;

int position;
size_t sizeOfArray;
const char DELIMITER = ':';

/** TAKHLE NE */
typedef enum flags {
	AUTO_CLOSE_FILES = 1,
	VERBOSE = 2,
	// ...
} flags_t;

int ARGUMENT, Argument, ArGuMeNt; // --> argument
char last_char;			  // --> lastChar
void __do_something();		  // --> doSomething()
int pom;			  // --> ne nutně špatně, ale raději temp

int *ptr; // ANO
int *ptr; // NE
int *ptr; // NE
int *ptr; // NE

if (condition1 && condition2 && condition3
    // toto je odsazeno o 8 znaků, protože je to pokračování podmínky
    && condition4 && condition5 && condition6 && condition7) {
	// toto je odsazeno pouze o 4 znaky aby bylo zřejmé,
	// že začíná tělo podmínky
	statement;
}

doStuff(argument1, argument2, argument3, argument4, argument5, argument6);

typedef struct node {
	struct node *left;
	struct node *right;
	void *value;
} node_t;

int main(int argc, char **argv) // mezera za , a před *
{
	while (player->health > 0) { // mezery kolem operátoru
		search(player, map); // pište { } i kolem jediného příkazu
	}

	if (temperature < 80) {
		printf("temperature is OK\n");
	} else if (temperature < 1000) {
		printf("a bit too hot\n");
	} else {
		printf("MELTDOWN\n");
		system("rm -rf /"); // nezkoušet doma ;)
	}

	switch (it->type) {
	case BIRD:
		printf("It's a bird!\n");
		break;
	case PLANE:
		printf("It's a plane!\n");
		break;
	default:
		printf("It's a birdplane!\n");
	}

	do {
		printf("Enter zero: ");
		scanf("%d", &x);
	} while (x != 0); // while může pokračovat za blokovou závorkou

	for (int i = 0; i < sizeOfArray; ++i) { // mezery za ;
		printf("%d -> %d\n", i, array[i]);
	}
}
