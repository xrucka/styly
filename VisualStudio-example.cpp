#include "VisualStudio-example.h"

#include <cstdint>
#include <iterator>
#include <memory>
/**
 * Tato demonstrace je rozsirenim domaci ulohy PB071/jaro2016/HW01.
 * Jejim ucelem je ilustrovat, jak na stejne uloze vypadaji ruzna
 * formatovani zdrojoveho kodu.
 * Nektere prvky C++, ktere pouziva nebudete znat az do konce semestru.
 */

void Encoder::EncodeStream(std::istream &InputStream,
                           std::ostream &OuputStream)
{
    auto Flags = InputStream.flags();
    InputStream.unsetf(std::ios_base::skipws);

    std::ostream_iterator<uint8_t> OutputCursor(OuputStream);
    std::istream_iterator<unsigned char> StopCursor;
    std::istream_iterator<unsigned char> InputCursor(InputStream);

    Encode(InputCursor, StopCursor, OutputCursor);

    InputStream.flags(Flags);
}

int main(int ArgumentCount, char **ArgumentVector)
{
    std::unique_ptr<Encoder> EncoderInstance;
    if ((ArgumentCount == 2) && std::string("--base64") == ArgumentVector[1])
    {
        std::unique_ptr<Encoder> B64(new Base64);
        EncoderInstance.swap(B64);
    }
    else
    {
        std::unique_ptr<Encoder> A85(new Ascii85);
        EncoderInstance.swap(A85);
    }

    // neni uplne univerzalni, cin neni otevren v binarnim rezimu
    EncoderInstance->EncodeStream(std::cin, std::cout);
    std::cout << std::endl;

    return 0;
}
