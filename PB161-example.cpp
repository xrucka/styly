#include "WebKit-example.h"

#include <cstdint>
#include <iterator>
#include <memory>
/**
 * Tato demonstrace je rozsirenim domaci ulohy PB071/jaro2016/HW01.
 * Jejim ucelem je ilustrovat, jak na stejne uloze vypadaji ruzna
 * formatovani zdrojoveho kodu.
 * Nektere prvky C++, ktere pouziva nebudete znat az do konce semestru.
 */

void Encoder::encodeStream(std::istream &inputStream,
                           std::ostream &outputStream) {
  auto flags = inputStream.flags();
  inputStream.unsetf(std::ios_base::skipws);

  std::ostream_iterator<uint8_t> writeCursor(outputStream);
  std::istream_iterator<unsigned char> stopCursor;
  std::istream_iterator<unsigned char> readCursor(inputStream);

  encode(readCursor, stopCursor, writeCursor);

  inputStream.flags(flags);
}

int main(int argc, char **argv) {
  std::unique_ptr<Encoder> encoderInstance;
  if ((argc == 2) && std::string("--base64") == argv[1]) {
    std::unique_ptr<Encoder> b64(new base64);
    encoderInstance.swap(b64);
  } else {
    std::unique_ptr<Encoder> a85(new ascii85);
    encoderInstance.swap(a85);
  }

  // neni uplne univerzalni, cin neni otevren v binarnim rezimu
  encoderInstance->encodeStream(std::cin, std::cout);
  std::cout << std::endl;

  return 0;
}
