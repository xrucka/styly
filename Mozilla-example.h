#ifndef PB161_EXAMPLE_H
#define PB161_EXAMPLE_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iostream>

class Encoder {
private:
  const unsigned mBytesIn;
  const unsigned mBytesOut;
  const unsigned mModulo;

  virtual uint8_t CodeByte(uint8_t) const = 0;
  virtual uint8_t GetPaddingPlaceholder() const = 0;

protected:
  Encoder(unsigned aBytesIn, unsigned aBytesOut, unsigned aModulo)
      : mBytesIn(aBytesIn), mBytesOut(aBytesOut), mModulo(aModulo) {}

public:
  void EncodeStream(std::istream &, std::ostream &);

  template <typename InputIterator, typename OutputIterator>
  void Encode(InputIterator aReadCursor, const InputIterator &aStopCursor,
              OutputIterator aWriteCursor) {
    do {
      uint64_t blockIbn = 0;
      uint8_t blockOut[mBytesOut];
      std::fill(blockOut, blockOut + mBytesOut, 0);

      // nacte nejvyse i znaku ze vstupu
      unsigned inputCounter = 0;
      unsigned bytesPadded = 0;
      for (inputCounter = 0;
           (inputCounter < mBytesIn) && (aReadCursor != aStopCursor);
           ++inputCounter)
        blockIbn = (blockIbn << 8) | static_cast<uint8_t>(*(aReadCursor++));

      // zBytek doplni 0 a zapamatuje si, kolik doplnil
      bytesPadded = mBytesIn - inputCounter;
      for (unsigned i = 0; i < bytesPadded; ++i)
        blockIbn <<= 8;

      const unsigned realInput = mBytesIn - bytesPadded;
      const unsigned bytesToPad =
          mBytesOut - ((realInput * mBytesOut + mBytesIn - 1) / mBytesIn);

      unsigned outputPosition = 0;
      // korektne zpracovany vystup ponacita
      for (outputPosition = 0; outputPosition < mBytesOut; ++outputPosition) {
        blockOut[mBytesOut - (outputPosition + 1)] =
            CodeByte(blockIbn % mModulo);
        blockIbn /= mModulo;
      }
      // pozice odpovidajici doplnovanym datum predvyplni
      for (outputPosition = 0; outputPosition < bytesToPad; ++outputPosition) {
        blockOut[mBytesOut - (outputPosition + 1)] = GetPaddingPlaceholder();
      }

      for (unsigned i = 0; i < mBytesOut; ++i)
        *(aWriteCursor++) = blockOut[i];

    } while (aReadCursor != aStopCursor);
  }
};

struct Base64 : public Encoder {
  Base64() : Encoder(3, 4, 64) {}

private:
  uint8_t CodeByte(uint8_t aByte) const {
    assert(aByte < 64);
    const uint8_t specCases[] = {'+', '/'};
    // following function implements the Base64 encoding table
    if (aByte < 26) {
      return aByte - 0 + 'A';
    } else if (aByte < 52) {
      return aByte - 26 + 'a';
    } else if (aByte < 62) {
      return aByte - 52 + '0';
    } else {
      return specCases[aByte - 62];
    }
  }
  uint8_t GetPaddingPlaceholder() const { return '='; }
};

struct Ascii85 : public Encoder {
  Ascii85() : Encoder(4, 5, 85) {}

private:
  uint8_t CodeByte(uint8_t aByte) const { return aByte + 33; }
  uint8_t GetPaddingPlaceholder() const { return CodeByte(0); }
};

#endif // PB161_EXAMPLE_H
