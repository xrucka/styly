#include "Mozilla-example.h"

#include <cstdint>
#include <iterator>
#include <memory>
/**
 * Tato demonstrace je rozsirenim domaci ulohy PB071/jaro2016/HW01.
 * Jejim ucelem je ilustrovat, jak na stejne uloze vypadaji ruzna
 * formatovani zdrojoveho kodu.
 * Nektere prvky C++, ktere pouziva nebudete znat az do konce semestru.
 */

void Encoder::EncodeStream(std::istream &aInputStream,
                           std::ostream &aOuputStream) {
  auto flags = aInputStream.flags();
  aInputStream.unsetf(std::ios_base::skipws);

  std::ostream_iterator<uint8_t> outputCursor(aOuputStream);
  std::istream_iterator<unsigned char> stopCursor;
  std::istream_iterator<unsigned char> inputCursor(aInputStream);

  Encode(inputCursor, stopCursor, outputCursor);

  aInputStream.flags(flags);
}

int main(int aArgumentCount, char **aArgumentVector) {
  std::unique_ptr<Encoder> EncoderInstance;
  if ((aArgumentCount == 2) && std::string("--base64") == aArgumentVector[1]) {
    std::unique_ptr<Encoder> B64(new Base64);
    EncoderInstance.swap(B64);
  } else {
    std::unique_ptr<Encoder> A85(new Ascii85);
    EncoderInstance.swap(A85);
  }

  // neni uplne univerzalni, cin neni otevren v binarnim rezimu
  EncoderInstance->EncodeStream(std::cin, std::cout);
  std::cout << std::endl;

  return 0;
}
