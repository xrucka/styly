#ifndef PB161_EXAMPLE_H
#define PB161_EXAMPLE_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iostream>

class Encoder {
private:
  const unsigned bytesIn;
  const unsigned bytesOut;
  const unsigned modulo;

  virtual uint8_t codeByte(uint8_t) const = 0;
  virtual uint8_t getPaddingPlaceholder() const = 0;

protected:
  Encoder(unsigned _bytesIn, unsigned _bytesOut, unsigned _modulo)
      : bytesIn(_bytesIn), bytesOut(_bytesOut), modulo(_modulo) {}

public:
  void encodeStream(std::istream &, std::ostream &);

  template <typename InputIterator, typename OutputIterator>
  void encode(InputIterator readCursor, const InputIterator &stopCursor,
              OutputIterator writeCursor) {
    do {
      uint64_t blockIn = 0;
      uint8_t blockOut[bytesOut];
      std::fill(blockOut, blockOut + bytesOut, 0);

      // nacte nejvyse i znaku ze vstupu
      unsigned inputCounter = 0;
      unsigned bytesPadded = 0;
      for (inputCounter = 0;
           (inputCounter < bytesIn) && (readCursor != stopCursor);
           ++inputCounter)
        blockIn = (blockIn << 8) | static_cast<uint8_t>(*(readCursor++));

      // zbytek doplni 0 a zapamatuje si, kolik doplnil
      bytesPadded = bytesIn - inputCounter;
      for (unsigned i = 0; i < bytesPadded; ++i)
        blockIn <<= 8;

      const unsigned realInput = bytesIn - bytesPadded;
      const unsigned bytesToPad =
          bytesOut - ((realInput * bytesOut + bytesIn - 1) / bytesIn);

      unsigned outputPosition = 0;
      // korektne zpracovany vystup ponacita
      for (outputPosition = 0; outputPosition < bytesOut; ++outputPosition) {
        blockOut[bytesOut - (outputPosition + 1)] = codeByte(blockIn % modulo);
        blockIn /= modulo;
      }
      // pozice odpovidajici doplnovanym datum predvyplni
      for (outputPosition = 0; outputPosition < bytesToPad; ++outputPosition) {
        blockOut[bytesOut - (outputPosition + 1)] = getPaddingPlaceholder();
      }

      for (unsigned i = 0; i < bytesOut; ++i)
        *(writeCursor++) = blockOut[i];

    } while (readCursor != stopCursor);
  }
};

struct Base64 : public Encoder {
  Base64() : Encoder(3, 4, 64) {}

private:
  uint8_t codeByte(uint8_t byte) const {
    assert(byte < 64);
    const uint8_t specialCases[] = {'+', '/'};
    // following function implements the base64 encoding table
    if (byte < 26) {
      return byte - 0 + 'A';
    } else if (byte < 52) {
      return byte - 26 + 'a';
    } else if (byte < 62) {
      return byte - 52 + '0';
    } else {
      return specialCases[byte - 62];
    }
  }
  uint8_t getPaddingPlaceholder() const { return '='; }
};

struct Ascii85 : public Encoder {
  Ascii85() : Encoder(4, 5, 85) {}

private:
  uint8_t codeByte(uint8_t byte) const { return byte + 33; }
  uint8_t getPaddingPlaceholder() const { return codeByte(0); }
};

#endif // PB161_EXAMPLE_H
