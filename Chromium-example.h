#ifndef PB161_EXAMPLE_H
#define PB161_EXAMPLE_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iostream>

class Encoder {
private:
  const unsigned bytes_in_;
  const unsigned bytes_out_;
  const unsigned modulo_;

  virtual uint8_t CodeByte(uint8_t) const = 0;
  virtual uint8_t GetPaddingPlaceholder() const = 0;

protected:
  Encoder(unsigned bytes_in, unsigned bytes_out, unsigned modulo)
      : bytes_in_(bytes_in), bytes_out_(bytes_out), modulo_(modulo) {}

public:
  void EncodeStream(std::istream &, std::ostream &);

  template <typename InputIterator, typename OutputIterator>
  void Encode(InputIterator read_cursor, const InputIterator &stop_cursor,
              OutputIterator write_cursor) {
    do {
      uint64_t block_in = 0;
      uint8_t block_out[bytes_out_];
      std::fill(block_out, block_out + bytes_out_, 0);

      // nacte nejvyse i znaku ze vstupu
      unsigned input_counter = 0;
      unsigned bytes_padded = 0;
      for (input_counter = 0;
           (input_counter < bytes_in_) && (read_cursor != stop_cursor);
           ++input_counter)
        block_in = (block_in << 8) | static_cast<uint8_t>(*(read_cursor++));

      // zbytek doplni 0 a zapamatuje si, kolik doplnil
      bytes_padded = bytes_in_ - input_counter;
      for (unsigned i = 0; i < bytes_padded; ++i)
        block_in <<= 8;

      const unsigned real_input = bytes_in_ - bytes_padded;
      const unsigned bytes_to_pad =
          bytes_out_ - ((real_input * bytes_out_ + bytes_in_ - 1) / bytes_in_);

      unsigned output_position = 0;
      // korektne zpracovany vystup ponacita
      for (output_position = 0; output_position < bytes_out_;
           ++output_position) {
        block_out[bytes_out_ - (output_position + 1)] =
            CodeByte(block_in % modulo_);
        block_in /= modulo_;
      }
      // pozice odpovidajici doplnovanym datum predvyplni
      for (output_position = 0; output_position < bytes_to_pad;
           ++output_position) {
        block_out[bytes_out_ - (output_position + 1)] = GetPaddingPlaceholder();
      }

      for (unsigned i = 0; i < bytes_out_; ++i)
        *(write_cursor++) = block_out[i];

    } while (read_cursor != stop_cursor);
  }
};

class Base64 : public Encoder {
public:
  Base64() : Encoder(3, 4, 64) {}

private:
  uint8_t CodeByte(uint8_t byte) const {
    assert(byte < 64);
    const uint8_t special_cases[] = {'+', '/'};
    // following function implements the base64 encoding table
    if (byte < 26) {
      return byte - 0 + 'A';
    } else if (byte < 52) {
      return byte - 26 + 'a';
    } else if (byte < 62) {
      return byte - 52 + '0';
    } else {
      return special_cases[byte - 62];
    }
  }
  uint8_t GetPaddingPlaceholder() const { return '='; }
};

class Ascii85 : public Encoder {
public:
  Ascii85() : Encoder(4, 5, 85) {}

private:
  uint8_t CodeByte(uint8_t byte) const { return byte + 33; }
  uint8_t GetPaddingPlaceholder() const { return CodeByte(0); }
};

#endif // PB161_EXAMPLE_H
