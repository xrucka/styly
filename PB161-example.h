#ifndef PB161_EXAMPLE_H
#define PB161_EXAMPLE_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iostream>

class Encoder {
private:
  const unsigned m_bytesIn;
  const unsigned m_bytesOut;
  const unsigned m_modulo;

  virtual uint8_t codeByte(uint8_t) const = 0;
  virtual uint8_t getPaddingPlaceholder() const = 0;

protected:
  Encoder(unsigned bytesIn, unsigned bytesOut, unsigned modulo)
      : m_bytesIn(bytesIn), m_bytesOut(bytesOut), m_modulo(modulo) {}

public:
  void encodeStream(std::istream &, std::ostream &);

  template <typename InputIterator, typename OutputIterator>
  void encode(InputIterator readCursor, const InputIterator &stopCursor,
              OutputIterator writeCursor) {
    do {
      uint64_t blockIn = 0;
      uint8_t blockOut[m_bytesOut];
      std::fill(blockOut, blockOut + m_bytesOut, 0);

      // nacte nejvyse i znaku ze vstupu
      unsigned inputCounter = 0;
      unsigned bytesPadded = 0;
      for (inputCounter = 0;
           (inputCounter < m_bytesIn) && (readCursor != stopCursor);
           ++inputCounter)
        blockIn = (blockIn << 8) | static_cast<uint8_t>(*(readCursor++));

      // zbytek doplni 0 a zapamatuje si, kolik doplnil
      bytesPadded = m_bytesIn - inputCounter;
      for (unsigned i = 0; i < bytesPadded; ++i)
        blockIn <<= 8;

      const unsigned realInput = m_bytesIn - bytesPadded;
      const unsigned bytesToPad =
          m_bytesOut - ((realInput * m_bytesOut + m_bytesIn - 1) / m_bytesIn);

      unsigned outputPosition = 0;
      // korektne zpracovany vystup ponacita
      for (outputPosition = 0; outputPosition < m_bytesOut; ++outputPosition) {
        blockOut[m_bytesOut - (outputPosition + 1)] =
            codeByte(blockIn % m_modulo);
        blockIn /= m_modulo;
      }
      // pozice odpovidajici doplnovanym datum predvyplni
      for (outputPosition = 0; outputPosition < bytesToPad; ++outputPosition) {
        blockOut[m_bytesOut - (outputPosition + 1)] = getPaddingPlaceholder();
      }

      for (unsigned i = 0; i < m_bytesOut; ++i)
        *(writeCursor++) = blockOut[i];

    } while (readCursor != stopCursor);
  }
};

struct base64 : public Encoder {
  base64() : Encoder(3, 4, 64) {}

private:
  uint8_t codeByte(uint8_t byte) const {
    assert(byte < 64);
    const uint8_t specialCases[] = {'+', '/'};
    // following function implements the base64 encoding table
    if (byte < 26) {
      return byte - 0 + 'A';
    } else if (byte < 52) {
      return byte - 26 + 'a';
    } else if (byte < 62) {
      return byte - 52 + '0';
    } else {
      return specialCases[byte - 62];
    }
  }
  uint8_t getPaddingPlaceholder() const { return '='; }
};

struct ascii85 : public Encoder {
  ascii85() : Encoder(4, 5, 85) {}

private:
  uint8_t codeByte(uint8_t byte) const { return byte + 33; }
  uint8_t getPaddingPlaceholder() const { return codeByte(0); }
};

#endif // PB161_EXAMPLE_H
