#include "Chromium-example.h"

#include <cstdint>
#include <iterator>
#include <memory>
/**
 * Tato demonstrace je rozsirenim domaci ulohy PB071/jaro2016/HW01.
 * Jejim ucelem je ilustrovat, jak na stejne uloze vypadaji ruzna
 * formatovani zdrojoveho kodu.
 * Nektere prvky C++, ktere pouziva nebudete znat az do konce semestru.
 */

void Encoder::EncodeStream(std::istream &input_stream,
                           std::ostream &output_stream) {
  auto flags = input_stream.flags();
  input_stream.unsetf(std::ios_base::skipws);

  std::ostream_iterator<uint8_t> output_cursor(output_stream);
  std::istream_iterator<unsigned char> end_cursor;
  std::istream_iterator<unsigned char> input_cursor(input_stream);

  Encode(input_cursor, end_cursor, output_cursor);

  input_stream.flags(flags);
}

int main(int argc, char **argv) {
  std::unique_ptr<Encoder> encoder_instance;
  if ((argc == 2) && std::string("--base64") == argv[1]) {
    std::unique_ptr<Encoder> b64(new Base64);
    encoder_instance.swap(b64);
  } else {
    std::unique_ptr<Encoder> a85(new Ascii85);
    encoder_instance.swap(a85);
  }

  // neni uplne univerzalni, cin neni otevren v binarnim rezimu
  encoder_instance->EncodeStream(std::cin, std::cout);
  std::cout << std::endl;

  return 0;
}
